/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import { Position, Range, TextDocumentPositionParams } from "vscode-languageserver/node";
import StringUtils from "./StringUtils";
import { Line, NewLine, NewScope, ScopeType } from "../project/Project";

interface TokenPosition {
	start: number;
	end: number;
	length: number;
}

export default class LineUtils {

	/**
	 * Given a Starting Line Number Read Backwards looking for Remarks
	 *
	 * @param lines
	 */
	public static getRemarksAboveLine(lines:Line[], lineNumber:number):string|undefined {

		var remark = undefined;
		var beg = -1;
		var end = -1;

		if (lineNumber < 0) return remark;

		var _line = lines[lineNumber].text.trim(); 
		var possibleLineComment = _line.indexOf('//');

		if (possibleLineComment > 0) {
			return _line.slice(possibleLineComment + 2).trim();
		}
		while (lineNumber > 0) {

			lineNumber -= 1;

			if(!lines[lineNumber].text)
				continue;

			_line = lines[lineNumber].text.trim();

			if (end < 0) {
				if (_line.indexOf("*/") >= 0) {
					end = lineNumber;
				} else if (_line.slice(0,2) == "//") {
					if (_line.replace(/[\/\-=*#_\s]/g,"") == "") break;
					//detect a possible outcomment of the same symbol delaration
					let lineComment = _line.slice(2).trim();
					if(lineComment[0] !== '.') {
						remark = lineComment;
						break;
					}
				} else if(_line !== "") {
				//stop at any normal code in between		
					break;
				}
			}

			if (beg < 0) {
				if (_line.indexOf("/*") >= 0) {
					beg = lineNumber;
					break;
				}
			}
		}

		if (beg >= 0 && end >= 0) {
			remark = "";
			var remarkLine;
			for (var i = beg; i <= end; i++) {
				remarkLine = lines[i].text;
				if(i == beg){
					remarkLine = remarkLine.slice(remarkLine.indexOf("/*")+2);
				}
				if(i == end){
					remarkLine = remarkLine.slice(0,remarkLine.indexOf("*/"));
				}
				remarkLine = remarkLine.trim();					
				if (remarkLine[0] == '*') {
					remarkLine = remarkLine.slice(1);
				}
				// if(remarkLine!=="") {
				// 	remark += remarkLine + "\n";
				// }
				remark += remarkLine + "\n";
			}
		}

		return remark;
	}

	public static getTokenAtSourcePosition2(sourceLines: string[] | undefined, line: number, column: number): string | undefined{
		if (sourceLines && sourceLines.length > line) {
			// Find the char and the surrounding symbol it relates to
			const sourceLine = LineUtils.removeComments(sourceLines[line]);
			return LineUtils.getTokenAtLinePosition(sourceLine, column);
		}
		return undefined;
	}

	public static getTokenAtLinePosition2(sourceLine: string | undefined, column: number): string | undefined {

		if(!sourceLine) return undefined;

		const tokens = StringUtils.splitIntoTokens(sourceLine.replace(/[=\-\+]/g," $& "));

		var tL = tokens.length;
		if (tL === 0) return undefined;

		if (tL === 1) return tokens[0];

		for (var i = 0; i < tL; i++) {
			var pos = sourceLine.indexOf(tokens[i]);
			if (pos > column) return tokens[i - 1];
		}

		return tokens[tL - 1];
	}

	/**
	 * Given a list of lines, returns what is the assumed symbol/label/value at a specific position
	 */
	public static getTokenAtSourcePosition(sourceLines: string[] | undefined, line: number, column: number): string | undefined {
		if (sourceLines && sourceLines.length > line) {
			// Find the char and the surrounding symbol it relates to
			const sourceLine = LineUtils.removeComments(sourceLines[line]);
			return LineUtils.getTokenAtLinePosition(sourceLine, column);
		}

		return undefined;
	}

	/**
	 * Given a line, returns what is the assumed symbol/label/value at a specific position
	 */
	public static getTokenAtLinePosition(sourceLine: string | undefined, column: number): string | undefined {
		if (sourceLine && column <= sourceLine.length) {
			let targetRegex = new RegExp("^.{0," + Math.max(column, 0) + "}\\b([\\w.]*)\\b.*$");
			let targetMatch = sourceLine.match(targetRegex);
			if (!targetMatch || !targetMatch[1]) {
				// Fallback: this regex is more lenient, so we can have rename working from the end of the string...
				// but it may give false positives
				targetRegex = new RegExp("^.{0," + Math.max(column - 1, 0) + "}\\b([\\w.]*)\\b.*$");
				targetMatch = sourceLine.match(targetRegex);
			}
			if (targetMatch && targetMatch[1]) {
				return targetMatch[1].split(".")[0];
			}
		}

		return undefined;
	}

	/**
	 * Given a line and a token, returns the location in that line (start and end) that the token is in
	 * A `character` parameter can be used when the token needs to be in that position
	 */
	public static getTokenPosition(line: string, token: string, character: number = -1): TokenPosition | undefined {
		const len = token.length;
		let pos = line.indexOf(token);
		while (pos > -1) {
			if (character < 0 || (pos <= character && pos + len >= character)) {
				return { start: pos, end: pos + len, length: len };
			}
			pos = line.indexOf(token, pos + len);
		}
		return undefined;
	}

	/**
	 * Given a line and a token, returns all the location in that line (start and end) that the token is in
	 */
	public static getTokenPositions(line: string, token: string): TokenPosition[] {
		const len = token.length;
		let pos = line.indexOf(token);
		const positions = [];
		while (pos > -1) {
			positions.push({ start: pos, end: pos + len, length: len });
			pos = line.indexOf(token, pos + len);
		}
		return positions;
	}

	/**
	 * Same as getTokenPosition(), but returning a range of a specific line
	 */
	public static getTokenRange(line: string, token: string, lineNumber: number, character: number = -1): Range | undefined {
		const pos = LineUtils.getTokenPosition(line, token, character);
		if (pos) {
			return Range.create(Position.create(lineNumber, pos.start), Position.create(lineNumber, pos.end));
		}
		return undefined;
	}

	/**
	 * Returns a line without comments
	 */
	public static removeComments(line: string): string | undefined {
		const removeCommentsRegex = /^(.+?)(\/\/.+|)$/;
		const sourceLineNoCommentsMatch = line.match(removeCommentsRegex);
		if (sourceLineNoCommentsMatch && sourceLineNoCommentsMatch[1]) {
			return sourceLineNoCommentsMatch[1];
		}
		return undefined;
	}

	/**
	 * Returns an array of Source Code Lines that are cleaned and Scoped.
	 * 
	 * @param lines 
	 */
	public static createSourceLines(lines: string[]): NewLine[] | undefined {

        let _lines = lines;
        let _cleanLines = this.cleanComments(_lines);
		// let _cleanLines = lines;
		let _newLines: NewLine[] = [];
		let _scopes: NewScope[] = [];
		let _parent: NewScope = null;
		let _scope: NewScope = null;
		let _skipNextBrace: boolean = false;
		let _namedLabel: string = undefined;
		let _scopeType: ScopeType = ScopeType.None;
		let _nextLevel = 0;
		let _nextName = '';



		_scope = <NewScope>{}
		_scope.name = "*"
		_scope.level = 0;
		_scope.type = ScopeType.Global;
		_parent = _scope;
		_scope.parent = _parent;
		_scope.type = ScopeType.None;

        var _label: RegExpMatchArray;

		_cleanLines.forEach(line => {

			console.log(line);

			let _line = line.trim();

			var _newLine = <NewLine>{};
			_newLine.text = line;
			_newLine.scope = _scope;
			
			/*
				handle .filenamespace

				push current scope
				create new scope with filenamespace name
			*/
			if (_line.startsWith(".filenamespace")) {
				let _last = _scope;
				_scopeType = ScopeType.Filenamespace;
				_scopes.push(_scope);
                _label = _line.trim().slice(14).trim().match(/\w*/);
				_parent = _scope;
				_scope = <NewScope>{};
				_scope.name = _label[0];
				_scope.parent = _last;
				_scope.level = 0;
				_scope.type = _scopeType;
				_skipNextBrace = true;
			}

			/*
				handle .macro

				push current scope
				create new scope with macro name
			*/
			if (_line.startsWith(".macro")) {
				let _last = _scope;
				let _name = _line.slice(6).trim();
				_scopeType = ScopeType.Macro;
				_scopes.push(_scope);
				_name = _name.replace("@","");
                _label = _name.match(/\w*/);
				_parent = _scope;
				_scope = <NewScope>{};
				_scope.name = _label[0];
				_scope.parent = _last;
				_scope.level = 0;
				_scope.type = _scopeType;
				_skipNextBrace = true;
			}

			/*
				handle .pseudocommand

				push current scope
				create new scope with pseudocommand name
			*/
			if (_line.startsWith(".pseudocommand")) {
				let _last = _scope;
				let _name = _line.slice(14).trim();
				_scopeType = ScopeType.PseudoCommand;
				_scopes.push(_scope);
				_name = _name.replace("@","");
                _label = _name.match(/\w*/);
				_parent = _scope;
				_scope = <NewScope>{};
				_scope.name = _label[0];
				_scope.parent = _last;
				_scope.level = 0;
				_scope.type = _scopeType;
				_skipNextBrace = true;
			}

			/*
				handle .namespace

				push current scope
				create new scope with namespace name
			*/
			if (_line.startsWith(".namespace")) {
				let _last = _scope;
				let _name = _line.slice(10).trim();
				_scopeType = ScopeType.Namespace;
				_scopes.push(_scope);
				_name = _name.replace("@","");
                _label = _name.match(/\w*/);
				_parent = _scope;
				_scope = <NewScope>{};
				_scope.name = _label[0];
				_scope.parent = _last;
				_scope.level = 0;
				_scope.type = _scopeType;
				_parent = _scope;
				_skipNextBrace = true;
			}

			/*
				handle .label

				push current scope
				create new scope with namespace name
			*/
			if (_line.startsWith(".label")) {
				let _split = _line.split(' ');
				_nextName = _split[1];
				_scopeType = ScopeType.Label;
				_nextLevel = 0;
				_skipNextBrace = false;
			}

			/*
				Look for a named label

				We will only scope a named label
				if the developer also includes
				brackets.

				So we will create a potential name
				and then the first opening bracket
				will use the name defined here.

			*/
			let _nl = _line.indexOf(":");

			if (_nl > 0 && _scopeType != ScopeType.PseudoCommand) {
				_namedLabel = _line.trim();
				_nl = _namedLabel.indexOf(":");
				_namedLabel = _namedLabel.slice(0, _nl);
				_scopeType = ScopeType.NamedLabel;
				_skipNextBrace = false;
				_nextLevel = 0
			}

			/*
				look for opening brace

				at this point we have probably already identified the
				next scope level so we can simply add one to the level
				and push it to the stack
			*/

      let _openBracePos = line.lastIndexOf("{");

			if (_openBracePos >= 0) {

				if (_scopeType == ScopeType.None) {

					var _last = _scope;
					_scopes.push(_scope);
					_scope = <NewScope>{};
					_scope.name = _last.name;
					_scope.level = _last.level + 1;
					_scope.parent = _last;
					_scope.type = _scopeType;
					_parent = _scope;
				}

				if (!_skipNextBrace) {

					if (_scopeType == ScopeType.Namespace || _scopeType == ScopeType.Macro || _scopeType == ScopeType.Function || _scopeType == ScopeType.PseudoCommand) {

						let _level = _scope.level + 1;
						var _last = _scope;
						_scopes.push(_scope);
						_scope = <NewScope>{};
						_scope.name = _parent.name;
						_scope.level = _level;
						_scope.parent = _last;
						_scope.type = _scopeType;
						_parent = _scope;
					}

					if (_scopeType == ScopeType.NamedLabel) {

						var _last = _scope;
						_scopes.push(_scope);
						_scope = <NewScope>{};
						_scope.name = _namedLabel;
						_scope.level = _nextLevel;
						_scope.parent = _last;
						_scope.type = _scopeType;
						_parent = _scope;
					}

					if (_scopeType == ScopeType.Label) {

						var _last = _scope;
						_scopes.push(_scope);
						_scope = <NewScope>{};
						_scope.name = _nextName;
						_scope.level = _nextLevel;
						_scope.parent = _last;
						_scope.type = _scopeType;
						_parent = _scope;
					}

					_nextLevel += 1;
				}

				_skipNextBrace = false;
			}

			/*
				look for closing brace

				pop the stack for the last scope
			*/

      let _closeBracePos = line.lastIndexOf("}", _openBracePos);

			// fixes issue #152 -- only allow look for closing brace that is past opening brace
			// if (_closeBracePos >= 0 && _openBracePos >= 0 && _closeBracePos > _openBracePos) {
			if (_closeBracePos >= 0 && _closeBracePos > _openBracePos) {
					_scope = _scopes.pop();
				_parent = _scope.parent;
				_scopeType = _scope.type;
			}

			_newLines.push(_newLine);

		});

		return _newLines;
	}

	private static cleanComments(lines: string[]): string[] {
		
		let _inBlock = false;
		let _lines: string[] = [];

		lines.forEach(line => {

				// check for block comment

				let _blockStart = line.indexOf("/*");
				if (_blockStart >= 0) {
					_inBlock = true;
				}

				let _blockEnd = line.indexOf("*/");
				if (_blockEnd >= 0) {
					_inBlock = false;					
					line = "";
				}

				if (_inBlock) {
					line = "";
				}
				
				// check for line comment

				let _lineComment = line.indexOf("//");

				if (_lineComment >= 0) {
					line = line.substring(0, _lineComment);
				}

				_lines.push(line);

			});

		return _lines;
	}

	public static isCursorInRemark(lines: string[], textDocumentPosition: TextDocumentPositionParams): boolean {

		var _inRemark:boolean = false;

		var _triggerLine = lines[textDocumentPosition.position.line];
		var _triggerCharPos = textDocumentPosition.position.character - 1;
		var _triggerToken = StringUtils.GetWordAt(_triggerLine, _triggerCharPos);
		var _tokensLeft = StringUtils.getWordsBefore(_triggerLine, _triggerCharPos);

		var blockCommentStart = _triggerLine.indexOf('/*');
		var blockCommentEnd = _triggerLine.lastIndexOf('*/');

		if ((blockCommentStart < _triggerCharPos && blockCommentEnd > _triggerCharPos ) ||
			(_tokensLeft && _tokensLeft.filter(token => token.indexOf("//") !== -1).length > 0) ||
			_triggerToken.slice(0,2) == "//" ||
			_triggerToken == "/*"
			) {
			_inRemark = true;
		}

		var cl = textDocumentPosition.position.line - 1;

		while(cl>=0) {

			blockCommentStart = lines[cl].indexOf('/*');
			blockCommentEnd = lines[cl].lastIndexOf('*/');

			if (blockCommentStart < blockCommentEnd) break;
			if (blockCommentStart > blockCommentEnd) {
				_inRemark = true;
				break;
			}
			
			cl--;
		}

		return _inRemark;
	}
}
