/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

/*
    Class: ProjectManager

    	Manage a Project

    Remarks:

		The Manager is responsible for
		the handling of providers that are
		available on the Client.

*/

import SettingsProvider, { Settings }
	from "../providers/SettingsProvider";

import HoverProvider 
	from "../providers/HoverProvider";

import Project 
	from "./Project";

import { ProjectInfoProvider } 
	from "../providers/Provider";

import DiagnosticProvider 
	from "../providers/DiagnosticProvider";

import { readFileSync }
	from "fs";

import PathUtils 
	from "../utils/PathUtils";

import { createHash } 
	from "crypto";

import DocumentSymbolProvider 
	from "../providers/DocumentSymbolProvider";

import SignatureHelpProvider 
	from "../providers/SignatureHelpProvider";

import ReferencesProvider 
	from "../providers/ReferencesProvider";

import DefinitionProvider 
	from "../providers/DefinitionProvider";

import { TextDocument } 
	from "vscode-languageserver-textdocument";

import { InitializeResult, TextDocuments, InitializeParams, InitializedParams, DidChangeTextDocumentParams, DidOpenTextDocumentParams, DidSaveTextDocumentParams, Connection, DidCloseTextDocumentParams, TextDocumentSyncKind, TextDocumentItem } 
	from "vscode-languageserver/node";
	
import CompletionProvider 
	from '../providers/CompletionProvider';


export default class ProjectManager {

    private settingsProvider: SettingsProvider;
    private hoverProvider: HoverProvider;
    private diagnosticProvider: DiagnosticProvider;
    private documentSymbolProvider: DocumentSymbolProvider;
	private CompletionProvider: CompletionProvider;
    private signatureHelpProvider: SignatureHelpProvider;
    private referencesProvider: ReferencesProvider;
    private definitionProvider: DefinitionProvider;

    private projects: Project[];
    private connection: Connection;
    private timer: NodeJS.Timer;

    constructor(connection: Connection) {

        this.projects = [];
        this.connection = connection;

        //  setup project information provider
        const projectInfoProvider: ProjectInfoProvider = {
            getProject: this.getProject.bind(this),
            getSettings: this.getSettings.bind(this)
        };

        this.settingsProvider = new SettingsProvider(connection, projectInfoProvider);
        this.hoverProvider = new HoverProvider(connection, projectInfoProvider);
        this.diagnosticProvider = new DiagnosticProvider(connection, projectInfoProvider);
        this.documentSymbolProvider = new DocumentSymbolProvider(connection, projectInfoProvider);
        this.CompletionProvider = new CompletionProvider(connection, projectInfoProvider);
        this.signatureHelpProvider = new SignatureHelpProvider(connection, projectInfoProvider);
        this.referencesProvider = new ReferencesProvider(connection, projectInfoProvider);        
        this.definitionProvider = new DefinitionProvider(connection, projectInfoProvider);

		connection.onNotification("kickass/assemble", (p: any) => {
			console.log("received message");
            var kickAssSettings = this.settingsProvider.getSettings();
            var project = this.findProject(p.uri.external);
            this.projects.push(project);
			if (kickAssSettings.valid) {
                project.assemble(kickAssSettings, p.text, true);
            }
		});

        connection.onInitialize((params: InitializeParams): InitializeResult => {
            return {
                capabilities: {
                    textDocumentSync: TextDocumentSyncKind.Full,
                    hoverProvider: true,
                    documentSymbolProvider: true,
                    referencesProvider : true,
                    definitionProvider : true,
                    signatureHelpProvider: {
                        triggerCharacters: ["(",","]
                    },
                    completionProvider: {
                        resolveProvider: true,
                        triggerCharacters: ["#", ".", "<", ">", ",", "*", '"', "(", "!"],
                    }
                }
            };
        });

        connection.onInitialized((params: InitializedParams) => {
        });

        connection.onDidOpenTextDocument((open: DidOpenTextDocumentParams) => {

            var kickAssSettings = this.settingsProvider.getSettings();

            var project = new Project(open.textDocument.uri);
            project.connection = this.connection;
            this.projects.push(project);

			// always build when opening the file for the first time so that we get symbols

            if (kickAssSettings.valid) {
                project.assemble(kickAssSettings, open.textDocument.text, true);
                this.diagnosticProvider.process(open.textDocument.uri);
            }
        });

        connection.onDidChangeTextDocument((change: DidChangeTextDocumentParams) => {

            var kickAssSettings = this.settingsProvider.getSettings();
            
            var project = this.findProject(change.textDocument.uri);
            var source = change.contentChanges[0].text;
        
            project.setSource(source); // always update the source

            if (kickAssSettings.valid && kickAssSettings.autoAssembleTrigger.indexOf('onChange') !== -1) {

                if (this.timer) { 
                    clearTimeout(this.timer); 
                }

                this.timer = setTimeout(() => {
		            // console.log("[ProjectManager] onTimer");
                    project.assemble(kickAssSettings, source);
                    this.diagnosticProvider.process(change.textDocument.uri);
                },                
                kickAssSettings.autoAssembleTriggerDelay);
            }
        });

        connection.onDidSaveTextDocument((change: DidSaveTextDocumentParams) => {

            var kickAssSettings = this.settingsProvider.getSettings();

            var project = this.findProject(change.textDocument.uri);
            var file = readFileSync(PathUtils.uriToPlatformPath(change.textDocument.uri), 'utf8');

            if (kickAssSettings.valid && kickAssSettings.autoAssembleTrigger.indexOf('onSave') !== -1) {
                project.assemble(kickAssSettings, file, true);
                this.diagnosticProvider.process(change.textDocument.uri);
            }

            if (this.timer) {
                clearTimeout(this.timer);
            }
        });

        connection.onDidCloseTextDocument((close: DidCloseTextDocumentParams) => {
            this.removeProject(close.textDocument.uri);
            if (this.timer) {
                clearTimeout(this.timer);
            }
        });
    }

    private findProject(uri: string): Project | undefined {
        var hash = createHash('md5').update(uri).digest('hex');
        for (var project of this.projects) {
            if (hash == project.getId()) {
                return project;
            }
        }
    }

    private removeProject(uri: string) {
        var pos = 0;
        var hash = createHash('md5').update(uri).digest('hex');
        for (var project of this.projects) {
            if (hash == project.getId()) {
                this.projects.splice(pos, 1);
            }
            pos += 1;
        }
    }

    public start() {
        this.connection.listen();
    }

    public getSettings(): Settings {
        return this.settingsProvider.getSettings();
    }

    public getProject(uri: string): Project {
        return this.findProject(uri);
    }

}