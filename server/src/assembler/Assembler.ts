/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import * as path
	from 'path';

import PathUtils
	from "../utils/PathUtils";

import { URI }
	from "vscode-uri";

import { Settings }
	from "../providers/SettingsProvider";

import { AssemblerInfo }
	from "./AssemblerInfo";

import { writeFileSync, readFileSync }
	from "fs";

import { spawnSync }
	from "child_process";

import { performance } 
	from 'perf_hooks';

export interface AssemblerResults {
    assemblerInfo: AssemblerInfo;
    stdout: string;
    stderr: string;
    status: number;
}

/*
    Class: Assembler

        Runs Kick to Assemble a Source File


    Remarks:

        Attempts to run KickAssembler, but will not actually
        create the assembled object. Instead it will produce
        an assembly information file, that can be used for
        diagnostic information about the source file being
        worked on.

*/
export class Assembler {

    // private assemblerResults: AssemblerResults;

    public assemble(settings: Settings, filename:string, text: string, ignoreOutputPathSetting: boolean = false, ignoreBuildStartup: boolean = false, ignoreText: boolean = false): AssemblerResults | undefined {

		// console.log('[Assembler] assemble')
		let _startTime = performance.now();

        let buildStartup: string = "";

        if (!ignoreBuildStartup)
            buildStartup = settings.startup;

        let uri:URI = URI.parse(filename);

        var outputDirectory: string = settings.assembler.option.outputDirectory;
        //Remove this when removing deprecated settings
        if(outputDirectory === '') {
            outputDirectory = settings.outputDirectory;
        }
        var sourcePath: string = PathUtils.getPathFromFilename(uri.fsPath);

        
        if (outputDirectory == "" || ignoreOutputPathSetting) {
            outputDirectory = sourcePath;
        }

        outputDirectory = path.resolve(outputDirectory);

        PathUtils.directoryCreate(outputDirectory);

        let javaOptions:string [] = [];

        var cpSeparator:string = process.platform == "win32" ? ';' : ':';
        var cpPlugins:string[] = settings.java.plugin.list;
        var cpPluginParameters:string[] = settings.java.plugin.properties;

        cpPluginParameters = cpPluginParameters.map(p => '-D' + p);

        // get the assembler jar
        let _assembler_jar = settings.assembler.jar;

        // get the assembler main class
        let _assembler_main = settings.assembler.main;

        javaOptions.push(
            "-cp",
            cpPlugins.join(cpSeparator) + cpSeparator + _assembler_jar,
            ...cpPluginParameters,
            _assembler_main
        );

        /*
            when assembling we want to take the
            source code that has been given to us
            and store it in a temporary source file
            first and then tell kickass to 
            use this

            the compile line ends up looking like:

                java -jar {.source.txt}
        */

        var tmpSource = path.join(sourcePath, ".source.txt");
        //tmpSource = path.resolve(tmpSource);

        let srcFilename = tmpSource;

        if(ignoreText){
            srcFilename = uri.fsPath;
        }

        /*
            when startup has been specified we want to
            use that file to assemble

            the compile ends up looking like:

                java -jar {startup}
        */

        if (buildStartup) {
            let buildSrcFilename = path.resolve() + path.sep + buildStartup;
            if(PathUtils.fileExists(buildSrcFilename)) {
                srcFilename = buildSrcFilename;
            }
        }

        writeFileSync(tmpSource, text);

        // if(srcFilename === tmpSource){
        //     writeFileSync(tmpSource, text);
        // }

        javaOptions.push(
            srcFilename,
            '-noeval',
            '-warningsoff',
            '-showmem',
        );

		if (buildStartup && !ignoreText) {
			javaOptions.push(
				'-replacefile',
				uri.fsPath,
				tmpSource

			);
		}

        /*
            the asminfo file is very important because
            it returns information about any errors
            in the source and also the list of symbols
            that are in the assembled source codes
        */

        var tmpAsmInfo = path.join(sourcePath, ".asminfo.txt");
        tmpAsmInfo = path.resolve(tmpAsmInfo);

        javaOptions.push(
            '-asminfo',
            'allSourceSpecific|version',
            '-asminfofile',
            tmpAsmInfo,
            '-asminfoToStdOut',
			'-afo'
        );

        if(settings.opcodes.DTV){
            javaOptions.push('-dtv');
        }
        if(!settings.opcodes.illegal){
            javaOptions.push('-excludeillegal');
        }

        settings.assemblerLibraryPaths.forEach((libPath) => {
            if(!path.isAbsolute(libPath)) {
                libPath = path.join(sourcePath, libPath);
            }
            if (PathUtils.directoryExists(libPath)) {
                javaOptions.push('-libdir',libPath);
            }
        }); 

        //  run java process and wait for return
        
        let _java_runtime = settings.java.runtime;

        // console.info(_java_runtime);
        // console.info(javaOptions);
        // console.info(path.resolve(sourcePath));

		let java = spawnSync(_java_runtime, javaOptions, { cwd: path.resolve(sourcePath) });

        /*
            KickAssembler >= 5.17 supports ASMINFO returned
            from stdout. we check here to see if that exists,
            otherwise drop back to reading it from the 
            file system.
        */

        let asminfoStartString:string = '### ASMINFO START ###';
        let asminfoEndString:string   = '### ASMINFO END ###';
        let asminfo_data:string = java.output.toString();
        let asminfoStart:number = asminfo_data.indexOf(asminfoStartString,0);

		// console.log(asminfo_data);

        if(asminfoStart > -1) {
            // console.info('- ASMINFO found in stdout');
            asminfoStart += asminfoStartString.length;
            asminfo_data  = asminfo_data.slice(asminfoStart,asminfo_data.indexOf(asminfoEndString));
        } else {
            // console.info('- ASMINFO missing from stdout');
            // console.info(tmpAsmInfo);
            asminfo_data = readFileSync(tmpAsmInfo, 'utf8');
        }

        // prepare assembler results

        var assemblerResults = <AssemblerResults>{};
        assemblerResults.assemblerInfo = new AssemblerInfo(asminfo_data, uri.fsPath);
        assemblerResults.stdout = java.stdout.toString();
        assemblerResults.stderr = java.stderr.toString();
        assemblerResults.status = java.status;

		let _endTime = performance.now();
		console.log(`assembler took ${_endTime - _startTime} milliseconds`);

		return assemblerResults;
    }

}