/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import { Provider, ProjectInfoProvider } from "./Provider";
import StringUtils from "../utils/StringUtils";

import {
	Connection,
	Location,
	ReferenceParams
} from "vscode-languageserver/node";
import { SymbolType } from '../project/Project';

export default class ReferencesProvider extends Provider {

    constructor(connection:Connection, projectInfo:ProjectInfoProvider) {
		super(connection, projectInfo);

		connection.onReferences((reference: ReferenceParams): Location[] => {

			if (projectInfo.getSettings().valid) {
				var project = projectInfo.getProject(reference.textDocument.uri);
				var lines = project.getSourceLines();
				var triggerLine = lines[reference.position.line];

				var locations: Location[] = [];

				var triggerCharacterPos = reference.position.character - 1;
				if (triggerCharacterPos < 0) triggerCharacterPos = 0;
				var token = StringUtils.GetWordAt(triggerLine.replace(/[#\.\+\-\*\/,<>]/g," "), triggerCharacterPos).trim();
				var _symbols = project.getSymbols()
				for(var symbol of _symbols) {

					if(symbol.name === token && symbol.type === SymbolType.Reference){
					// if(symbol.name === token) {
							locations.push(<Location> {
							uri: symbol.isMain ? project.getUri() : project.getSourceFiles()[symbol.fileIndex].getUri(),
							range: symbol.range
						});
					}
				}
				console.log(locations)
				return locations;
			}
		});
	}
}