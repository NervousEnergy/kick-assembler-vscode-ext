### Supported VSCode Protocols

VSCode = ^1.60
Language Server = 3.16.0
### Building A Release

The following command will publish a new release up to the VSCode Marketplace with the current Version in package file. It is important to have this updated before you release. You cannot re-publish the same release, you will receive an error.

```
> vsce publish --baseContentUrl https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/blob/master --baseImagesUrl https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/raw/master
```

### Building A Local Copy

A local copy will create a VSIX file that you can use for sharing with other people without publishing. It will let you install the extension locally on your computer. This is helpful to do when you are testing features our with others, or just want to make sure that the extension installs and works prior to publishing.

```
> vsce package --baseContentUrl https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/blob/master --baseImagesUrl https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/raw/master
```

### Develop and Test locally
#### Prerequisities
- Fork https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext
- Clone your fork locally
```
> git clone https://gitlab.com/<yourgitlabuser>/<yourfork>.git
```
- Install npm modules
```
> cd <yourfork>
> npm install
```

#### Debugging The Extension

- open the fork folder in VSCode itself
- select `compile` from the NPM Scripts section of the files Tab (or press crtl-shift-b) 
- change to the Debug-Tab and Start "Client+Server"
- another VSCode Window will open which uses your just compiled local fork extension
 
### Contributing Changes
We welcome changes to the Extension, so please consider sending us pull requests. As you do your development and commit your changes to GIT, please make sure to follow the guidlines for formatting your Commit messages:

https://juhani.gitlab.io/go-semrel-gitlab/commit-message/

While we will consider almost all FIX and FEATURE requests, we also ask that before you commit any BREAKING CHANGES, please consult with the main development team using the Issues Discussion interface available in [GitLab](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues).

#### Formatting Your Commit Messages
Please using the following format:

```type(scope): subject```

Where TYPE is one of the following:

- FEAT: new feature which is a minor change
- FIX, REFACTOR, PERF, DOCS, STYLE, TEST: small changes like patches
- BREAKING CHANGE: major change to the code

Where SCOPE is:

What part of the code is being changed, for example:

- ASSEMBLER: changes to the assembler
- EDITOR: changes to the editor
- RUN: changes to program running
- SETTINGS: changes to the settings
- HOVER: changes to the hover manager

If you are not sure what to put in the SCOPE, please ask one of the main developers.

Where SUBJECT is:

The description of the change being made. You may include shortcuts from GitLab to include Issues

For example:

```Fix(assembler): new -binfile option was not always present #43```

### Extension Resources

Here are some additional links to help you get started with understanding how to write extensions and language servers for Visual Studio Code.

- https://code.visualstudio.com/api/language-extensions/language-server-extension-guide
- https://github.com/Microsoft/vscode-extension-samples/tree/master/lsp-sample
