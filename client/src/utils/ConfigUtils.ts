/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/


import * as vscode from 'vscode';
// import * as fs from 'fs';
// import { Uri } from "vscode"; 
import PathUtils from './PathUtils';


export default class ConfigUtils {

    /*
        Validates that we have enough to at least
        Build the source file.

        We check for a proper Java configuration first, 
        and then we check for a valid KickAss jar file.
    */
	public static validateBuildSettings():boolean {

		let settings = vscode.workspace.getConfiguration("kickassembler");

        let _java_runtime: string = settings.get("java.runtime");
        if (!_java_runtime) {
            _java_runtime = settings.get("javaRuntime");
        }
		
		if (!PathUtils.fileExists(_java_runtime)) {
			var _valid = vscode.window.showErrorMessage("Could Not Find the Java Runtime from the java.runtime setting.", { title: 'Open Settings'});
            _valid.then((value) => {
                if (value){
                    vscode.commands.executeCommand('workbench.action.openSettings', `kickassembler.java.runtime`);
                }
            });
			return false;
		}

        // get the path to the kickass jar
        let _assembler_jar:string = settings.get("assembler.jar");

		if (!PathUtils.fileExists(_assembler_jar)) {
			var _valid = vscode.window.showErrorMessage("Could Not Find the KickAss Jar file.", { title: 'Open Settings'});
            _valid.then((value) => {
                if (value){
                    vscode.commands.executeCommand('workbench.action.openSettings', `kickassembler.assembler.jar`);
                }
            });
			return false;
		}

        return true;
	}

	public static validateRunSettings():boolean {

		let settings = vscode.workspace.getConfiguration("kickassembler");

        //  get emulator runtime
        let emulatorRuntime: string = settings.get("emulator.runtime");

        if (!emulatorRuntime) {
            emulatorRuntime = settings.get("emulatorRuntime");
        }

		if (!PathUtils.fileExists(emulatorRuntime)) {
			var _valid = vscode.window.showWarningMessage("Could Not Find the Emulator Runtime.", { title: 'Open Settings'});
            _valid.then((value) => {
                if (value){
                    vscode.commands.executeCommand('workbench.action.openSettings', `kickassembler.emulator.runtime`);
                }
            });
			return false;
		}

        return true;
	}

	public static validateDebugSettings(ignoreNull:boolean = true):boolean {

		let settings = vscode.workspace.getConfiguration("kickassembler");

        let debuggerRuntime: string = settings.get("debugger.runtime");
        if (!debuggerRuntime) {
            debuggerRuntime = settings.get("debuggerRuntime");
        }

		// don't bother checking if it is empty and we don't care
		if (ignoreNull && !debuggerRuntime) {
			return true;
		}

		if (!PathUtils.fileExists(debuggerRuntime)) {
			var _valid = vscode.window.showWarningMessage("Could Not Find the Debugger Runtime.", { title: 'Open Settings'});
            _valid.then((value) => {
                if (value){
                    vscode.commands.executeCommand('workbench.action.openSettings', `kickassembler.debugger.runtime`);
                }
            });
			return false;
		}

        return true;
	}

}