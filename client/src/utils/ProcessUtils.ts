import { spawn, SpawnOptions, spawnSync, StdioOptions } from "child_process";
import { window, Extension, extensions, OutputChannel} from 'vscode';

/**
 * Runs an external process
 */
export default class ProcessUtils {

    private output: OutputChannel;
    private extension : Extension<any>;

    constructor(output: OutputChannel){
        this.output = output;
        this.extension = extensions.getExtension('paulhocker.kick-assembler-vscode-ext');
    }

    public spawnSyncExternalProcess(processInfo: ProcessInfo, stdOutValidation?: Function) {
        this.logDiagnostics(processInfo);
        const [command, spawnOptions] = this.configureProcess(processInfo, 'pipe');
        
        const childProcess = spawnSync(command, processInfo.args, spawnOptions);
        const stdErr = childProcess.stderr.toString();

        if (stdErr) {
            throw new Error(stdErr);
        }   

        if(stdOutValidation && stdOutValidation()){
            throw new Error(childProcess.stdout.toString());
        }
    }

    public spawnExternalProcess(processInfo: ProcessInfo) {
        this.logDiagnostics(processInfo);
        
        const [command, spawnOptions] = this.configureProcess(processInfo, 'inherit');
        const childProcess = spawn(command, processInfo.args, spawnOptions);
        childProcess.unref();      
    }

    private logDiagnostics(processInfo: ProcessInfo) {
        if (processInfo.showDiagnostics) {
            this.output.show(true);
            this.output.appendLine("");
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine(`${this.extension.packageJSON.displayName} ${this.extension.packageJSON.version}`);
            this.output.appendLine("Diagnostics");
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine(`Mode             : ${processInfo.debugMode}`);
            this.output.appendLine(`Platform         : ${process.platform}`);
            this.output.appendLine(`Current Dir      : ${processInfo.currentWorkingDirectory}`);
            this.output.appendLine(`Runtime          : ${processInfo.command}`);
            this.output.appendLine(`Options          :`);
            this.output.appendLine("");
            for (var i = 0; i < processInfo.args.length; i++) {
                this.output.appendLine(`  ${processInfo.args[i]}`);
            }
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine("");
        }
    }

    private configureProcess(processInfo: ProcessInfo, stdio: StdioOptions) : [string, SpawnOptions] {
        var command: string;
        var spawnOptions : SpawnOptions;

        if (process.platform == "win32") {
            //  spawn child process for win32
            command = processInfo.command;
            spawnOptions = {
                detached: true,
                stdio: stdio,
                cwd: processInfo.currentWorkingDirectory,
                shell: false
            };
        } 
        else if (process.platform == "darwin") { 
            //  spawn child process for osx
            command = processInfo.command;

            if(processInfo.command.endsWith('.app')) {
                command = "open";
                processInfo.args = ["-n", "-a", processInfo.command, "--args", ...processInfo.args];
            }

            spawnOptions = {
                detached: true,
                stdio: stdio,
                cwd: processInfo.currentWorkingDirectory,
                shell: true
            };
        } else if (process.platform == "linux") {
            //  spawn child process for linux
            command = processInfo.command;
            spawnOptions = {
                detached: true,
                stdio: stdio,
                cwd: processInfo.currentWorkingDirectory,
                shell: false
            };
        } else {
            throw new Error(`Platform ${process.platform} is not Supported.`);
        }

        return [command, spawnOptions];
    }
}

export class ProcessInfo {
    public command: string;
    public args: string[];
    public currentWorkingDirectory: string
    public debugMode:string;
    public showDiagnostics: boolean;

    constructor(init?:Partial<ProcessInfo>) {
        Object.assign(this, init);
    }
}