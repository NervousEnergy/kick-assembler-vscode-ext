/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import { Uri, WorkspaceFolder, WorkspaceConfiguration, window, workspace, TextDocument, TextEditor, OutputChannel } from 'vscode';
import * as path from "path";
import PathUtils from './PathUtils';
import { fstat } from 'node:fs';
import { VersionedTextDocumentIdentifier } from 'vscode-languageserver-types';

export default class ClientUtils {

    /**
     * Return a Valid Uri for an Assembled Program
     * 
     * This helper method will create the valid
     * Uri for the program file that will be
     * assembled by KickAssembler.
     * 
     * 
     * Example 1:
     * 
     * With an Output setting of 
     * 
     *      "" (blank) 
     * 
     * and a source filename of 
     * 
     *      /home/user/workspace/coolgame.asm
     * 
     * this method will return
     * 
     *      /home/user/workspace/coolgame.prg
     * 
     * 
     * Example 2:
     * 
     * with an output of 
     * 
     *      "output"
     * 
     * and a source filename of
     * 
     *      /home/user/workspace/coolgame.asm
     * 
     * this method will return
     * 
     *      /home/user/workspace/output/coolgame.prg
     * 
     * 
     * Example 3:
     * 
     * with an output of
     * 
     *      "/home/user/build"
     * 
     * and a source filename of 
     * 
     *      /home/user/workspace/coolgame.asm
     * 
     * this method will return
     * 
     *      /home/user/build/coolgame.prg
     * 
     * 
     * Example 4:
     * 
     * with an output of
     * 
     *      "build"
     * 
     * and a source filename of 
     * 
     *      /home/user/workspace/module/coolmodule.asm
     * 
     * this method will return
     * 
     *      /home/user/workspace/build/coolgame.prg
     * 
     */
    public static GetWorkspaceProgramFilename():string {

        // get the output path
        // var outputPath:string = this.GetOutputPath(this.output);
        // var sourceFile: string = this.GetOpenDocumentUri().fsPath;

        // // get the final program filename
        // var prg = this.CreateProgramFilename(path.basename(sourceFile));

        // // build the filename
        // var outputFile:string = outputPath + path.sep + prg;

        // return outputFile;
        return;
    }

    public static CreateProgramFilename(filename:string): string | undefined {

        var _ext_name = ".prg";

        if (this.GetSettings().get("assembler.option.binfile")) {
            _ext_name = ".bin";
        }

        // simply replace the extension of the current file with `.prg`

        let _ext = filename.slice(filename.lastIndexOf('.'))
        filename = filename.replace(_ext, _ext_name);

        return filename;

    }

    /**
     * Return an Path for Output
     * 
     * This method will return a valid string
     * path for the output specified on the
     * extension setting
     * 
     *      kickassembler.output
     * 
     * To do this we basically get the root folder
     * of the Workspace and either add the output 
     * folder to it, or if it is an absolute path
     * make that the output folder.
     * 
     * Example 1:
     *  
     *  with an output setting of
     * 
     *      "output"
     * 
     *  and a workspace value of
     * 
     *      /home/user/workspace
     * 
     *  the returning value will be
     * 
     *      /home/user/workspace/output
     * 
     * Example 2:
     *  
     *  with an output setting of
     * 
     *      "/output"
     * 
     *  and a workspace value of
     * 
     *      /home/user/workspace
     * 
     *  the returning value will be
     * 
     *      /output
     * 
     * Example 3:
     *  
     *  with an output setting of
     * 
     *      "./output"
     * 
     *  and a workspace value of
     * 
     *      /home/user/workspace
     * 
     *  the returning value will be
     * 
     *      /home/user/workspace/output
     * 
     */
	public static GetOutputPath(output: OutputChannel):string {


        output.appendLine("O1")
        output.appendLine(`workspace folders: ${JSON.stringify(workspace.workspaceFolders)}`)
        output.appendLine(`workspace folder: ${this.GetWorkspaceFolder().uri.fsPath}`)
        output.appendLine(`output folder: ${this.GetSettings().get("assembler.option.outputDirectory")}`)
        output.appendLine(`keep hier: ${this.GetSettings().get("assembler.option.outputKeepSourceHierarchy")}`)

        var rootFolder:string = this.GetWorkspaceFolder().uri.fsPath;
        var outputDirectory:string = this.GetSettings().get("assembler.option.outputDirectory");
        var keepHier:string = this.GetSettings().get("assembler.option.outputKeepSourceHierarchy");
        //Remove this when removing deprecated settings
        output.appendLine("O2")
        if(outputDirectory === ''){
            outputDirectory = this.GetSettings().get("outputDirectory");
        }
        var outputPath:string;

        /*
            the default is to use the root workspace folder
        */
        outputPath = rootFolder;

        /*
            when there is something populated in the 
            output directory
        */
        output.appendLine("O3")
        if(!path.isAbsolute(outputDirectory)) {
            outputPath = path.join(rootFolder, outputDirectory);
        } else {
            outputPath = outputDirectory;
        }

        output.appendLine("O4")
        outputPath = path.normalize(outputPath);
        PathUtils.directoryCreate(outputPath);

        return outputPath;
    }

    /**
     * Return the Active Text Editor Uri
     * 
     * This method is a simple helper method
     * to retrieve the currently open 
     * editor file in the Workspace.
     * 
     */
    public static GetSourceUri():Uri {
        return window.activeTextEditor.document.uri;
    }

    public static GetOpenDocumentUri(nativeUri:Boolean = false): Uri | undefined {

        var _uri : Uri;
        let _document = this.GetOpenDocument();

        if (_document) {
            _uri = nativeUri ? _document.uri : Uri.parse(_document.fileName);
        }

        return _uri;
    }

    /**
     * Returns the Active Open Document
     */
    public static GetOpenDocument(): TextDocument | undefined {


        var _document : TextDocument;

        /*
            first try the active window text editor

            if it has a valid viewColumn (! undefined) then
            we can pretty safely assume it is one of the
            open source code windows
        */

        let activeEditor = window.activeTextEditor;

        // get the document and return it to the caller
        if (activeEditor != undefined) {
            if (activeEditor.viewColumn)
                _document = activeEditor.document;
        }

        let textEditors = window.visibleTextEditors;

        if (textEditors.length < 0) {
            _document = undefined;
        }

        if (!_document) {
            for (var i = 0; i < textEditors.length; i++ ) {
                var editor:TextEditor = textEditors[i];
                if (editor.viewColumn == 1) {
                    _document = editor.document;
                    // return document;
                }
            }
        }

        return _document;
    }

    public static GetStartupUri():Uri | undefined {

        // get the build master

        let buildStartup:string = this.GetSettings().get("startup");
        buildStartup = buildStartup.trim();

        var uri: Uri;

        if (buildStartup) {
            let filename = path.join(this.GetWorkspaceFolder().uri.fsPath, buildStartup);
            uri = Uri.parse(filename);
        }

        return uri;
    }

    public static GetStartupDocument(callback?:Function) {

        let _buildStartup:string = this.GetSettings().get("startup");
        _buildStartup = _buildStartup.trim();

        if (_buildStartup) {
            let filename = path.join(this.GetWorkspaceFolder().uri.fsPath, _buildStartup);
            workspace.openTextDocument(filename).then(function (response) {
                if(callback) callback(response);
            });
        } else if(callback) callback();

    }

    /**
     * Return The Extension Settings
     * 
     * A simple helper method to return
     * the settings for this extension
     * 
     *      kickassembler
     */
    public static GetSettings():WorkspaceConfiguration  {
		return workspace.getConfiguration("kickassembler");
    }

    /**
     * Return the Workspace Folder
     * 
     * Note: For now we are just returning the root folder. This will
     * need to be updated at some point to support multiple workspace
     * folders.
     * 
     */
    public static GetWorkspaceFolder():WorkspaceFolder {
        return workspace.workspaceFolders[0];
    }

    /**
     * Enclose a String with Quotes
     * 
     * @param text 
     * @returns 
     */
    public static EncloseWithQuotes(text: string): string | undefined {
        return `"${text}"`;
    }


    /**
     * Replace Option Variable String
     * 
     */
     public static ReplaceOptionVar(from: string, to: string, options: string[]) {    

        for (let i = 0; i < options.length; i++) {
            options[i] = options[i].replace(from, to);
        }
    }   

	public static getWorkspaceFolderPath(): string {

		const workspaceFolder = this.GetWorkspaceFolder();
		
		if (workspaceFolder) {
			return workspaceFolder.uri.fsPath;
		} else {
			return "";
		}

	}

}
