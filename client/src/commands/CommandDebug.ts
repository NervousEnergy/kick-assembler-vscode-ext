/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import { workspace, ExtensionContext } from 'vscode';
import ClientUtils from '../utils/ClientUtils';
import * as vscode from 'vscode';
import * as path from 'path';
import ProcessUtils, { ProcessInfo } from '../utils/ProcessUtils';
import PathUtils from '../utils/PathUtils';

export class CommandDebug {

    private configuration: vscode.WorkspaceConfiguration;
    private showDiagnostics:boolean = false;
    private processUtils: ProcessUtils;
    private output: vscode.OutputChannel

    public buildFilename : string;
    public viceSymbolFilename : string;
    public symbolFilename : string;

    constructor(context: ExtensionContext, output: vscode.OutputChannel) {
        this.configuration = workspace.getConfiguration('kickassembler');
        this.showDiagnostics = this.configuration.get("editor.showDiagnostics");
        this.buildFilename = null;
        this.viceSymbolFilename = null;
        this.symbolFilename = null;
        this.processUtils = new ProcessUtils(output);
        this.output = output;
    }

    public runOpen(text: vscode.TextDocument) {

        this.buildFilename = this.buildFilename.trim();

        /*
            when the path on the filename is not
            absolute then we want to add the
            source folder to it
        */

        if (!path.isAbsolute(this.buildFilename)) {
            this.buildFilename = ClientUtils.GetOutputPath(this.output)  + path.sep + this.buildFilename;
        }

        this.buildFilename = path.normalize(this.buildFilename);

        let _base = path.basename(this.buildFilename);
        let _path = path.dirname(this.buildFilename);

        /*
            fix vice symbol filename
        */

        // if (this.viceSymbolFilename) {
        
        //     if(!path.isAbsolute(this.viceSymbolFilename)) {
        //         this.viceSymbolFilename = ClientUtils.GetOutputPath(this.output) + path.sep + this.viceSymbolFilename;
        //     }
    
        //     this.viceSymbolFilename = path.normalize(this.viceSymbolFilename);
        // }
    
        // new output path stuff
        this.output.appendLine("----------------------------------------")
		const newSourcePath = PathUtils.GetPathFromFilename(text.uri.fsPath);
		this.output.appendLine(`Source Path:  ${newSourcePath}`);
		const newOutputDir:string = this.configuration.get("assembler.option.outputDirectory");
		this.output.appendLine(`Output Dir:   ${newOutputDir}`);
		const newOutputKeepHier:boolean= this.configuration.get("assembler.option.outputKeepsSourceHierarchy", false);
		this.output.appendLine(`Output Keep:  ${newOutputKeepHier}`);
        let newOutputPath = PathUtils.getOutputPath(ClientUtils.getWorkspaceFolderPath(), newSourcePath, newOutputDir, newOutputKeepHier)
		this.output.appendLine(`Output Path:  ${newOutputPath}`);
        let newOutputFilename = path.join(newOutputPath, ClientUtils.CreateProgramFilename(path.basename(text.fileName)));
		this.output.appendLine(`Output File:  ${newOutputFilename}`);
        let newSymbolFile = path.join(newOutputPath, this.symbolFilename);
		this.output.appendLine(`Symbol File:  ${newSymbolFile}`);
        let newViceSymbolFile = path.join(newOutputPath, this.viceSymbolFilename);
		this.output.appendLine(`Vice File:    ${newViceSymbolFile}`);
        this.output.appendLine("----------------------------------------")

        // if (this.buildFilename) {
        //     this.run(this.buildFilename, _path);
        // } else {
        //     let program = path.join(ClientUtils.GetOutputPath(this.output), ClientUtils.CreateProgramFilename(_base));
        //     this.run(program, _path);
        // }

        this.symbolFilename = newSymbolFile;
        this.viceSymbolFilename = newViceSymbolFile;
        this.run(newOutputFilename, newOutputPath);

    }

    public runStartup() {

        this.buildFilename = this.buildFilename.trim();

        /*
            when the path on the filename is not
            absolute then we want to add the
            source folder to it
        */

        if (!path.isAbsolute(this.buildFilename)) {
            this.buildFilename = ClientUtils.GetOutputPath(this.output)  + path.sep + this.buildFilename;
        }

        this.buildFilename = path.normalize(this.buildFilename);

        let _base = path.basename(this.buildFilename);
        let _path = path.dirname(this.buildFilename);

        /*
            fix vice symbol filename
        */

        if(!path.isAbsolute(this.viceSymbolFilename)) {
            this.viceSymbolFilename = ClientUtils.GetOutputPath(this.output) + path.sep + this.viceSymbolFilename;
        }

        this.viceSymbolFilename = path.normalize(this.viceSymbolFilename);

        if (this.buildFilename) {
            this.run(this.buildFilename, _path);
        } else {
            let program = path.join(ClientUtils.GetOutputPath(this.output), ClientUtils.CreateProgramFilename(_base));
            this.run(program, _path);
        }

    }

    private run(buildFilename:string, buildPath:string, debugMode:string = "open") {

        // is the emulator path set?
        let _debugger_runtime: string = this.configuration.get("debugger.runtime");
        if (!_debugger_runtime) {
            _debugger_runtime = this.configuration.get("debuggerRuntime");
        }

        // normalize path fix slashes, etc.
        _debugger_runtime = path.normalize(_debugger_runtime);

        // remove trailing slash
        if (_debugger_runtime.endsWith(path.sep)) {
            _debugger_runtime = _debugger_runtime.substring(0, _debugger_runtime.length - 1);
        }

        let _debugger_options_str: string = this.configuration.get("debugger.options");
        if (!_debugger_options_str) {
            _debugger_options_str = this.configuration.get("debuggerOptions");
        }

        if (!_debugger_options_str) {
            _debugger_options_str = "";
        }

        let _debugger_options: string[] = _debugger_options_str.match(/\S+/g) || [];

        // vice specific options

        let _vsf = "";
        let _use_vice_symbols: boolean = false; 
        _use_vice_symbols = this.configuration.get("assembler.option.viceSymbols") || this.configuration.get("emulatorViceSymbols");

        if (_use_vice_symbols) {
            if (this.viceSymbolFilename) {
                _vsf = this.viceSymbolFilename;
            }
        }

        // enclose in quotes to accomodate filenames with spaces on Mac

        if (process.platform == "darwin") {
            if (_debugger_runtime.search(" ") > 0) { _debugger_runtime = ClientUtils.EncloseWithQuotes(_debugger_runtime); }
            if (buildFilename.search(" ") > 0) { buildFilename = ClientUtils.EncloseWithQuotes(buildFilename); }
            if (_vsf.search(" ") > 0) { _vsf = ClientUtils.EncloseWithQuotes(_vsf); }
        }

        // handle replaceable variables
        ClientUtils.ReplaceOptionVar("${kickassembler:buildFilename}", buildFilename, _debugger_options);
        ClientUtils.ReplaceOptionVar("${kickassembler:viceSymbolsFilename}", _vsf, _debugger_options);

        // finalize debugger options

        let _options = _debugger_options.filter(function (el) {
            return el != "";
        })

        const processInfo = new ProcessInfo({
            command: _debugger_runtime,
            args: _options,
            currentWorkingDirectory: buildPath,
            debugMode: debugMode,
            showDiagnostics: this.showDiagnostics
        });
        
        this.processUtils.spawnExternalProcess(processInfo);
    }
}