![Visual Studio Marketplace Version](https://img.shields.io/visual-studio-marketplace/v/paulhocker.kick-assembler-vscode-ext.svg?style=flat-square)

![Visual Studio Marketplace Downloads](https://img.shields.io/visual-studio-marketplace/d/paulhocker.kick-assembler-vscode-ext.svg?style=flat-square)

![Visual Studio Marketplace Downloads](https://img.shields.io/visual-studio-marketplace/i/paulhocker.kick-assembler-vscode-ext.svg?style=flat-square)


# Kick Assembler 8-Bit Retro Studio
[Kick Assembler 8-Bit Retro Studio](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext) is an [open-source](https://marketplace.visualstudio.com/items?itemName=paulhocker.kick-assembler-vscode-ext) extension for [Visual Studio Code](https://code.visualstudio.com) that helps you build programs using the [Kick Assembler](http://www.theweb.dk/KickAssembler) assembler from [Mads Nielsen](https://www.facebook.com/groups/1452413708322976/user/689988657).

This extension **boosts** your `8-bit Retro Development` experience when using the awesome `Kick Assembler`. It helps you code, build, run and debug for many of the different 8-Bit systems using the `6502` family of Processors like `Commodore 64, Atari 2600, Atari Computers` and many more.

It is powerful, feature rich, and very customizable to meet your needs. Do you want to build for the `Commodore 64` &mdash; no problem, setup your emulator to run VICE when building. Want to create for the `Atari line of computers` &mdash; no problem, change your settings to use your favorite Atari emulator.

Not sure how to get started with some of the different systems out there &mdash; use our [guides](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/wikis/Guides) on how to configure and build for your favorite 6502 8-Bit system.

Use our [wiki](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/wikis/Home) for a full set of information on the different settings and options available.
# Submitting Bugs
If you would like to submit a bug, please use the [Trello Board](https://trello.com/b/vIsioueo/kick-assembler-vscode-ext) or create an issue on [GitLab](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues).
# Support
We have a Discord Channel setup to assist anyone using the Extension. Use this link to join.

[KickAss 8-Bit Retro Studio](https://discord.gg/Jxk3ptvRaw)

# Roadmap
[Trello Board](https://trello.com/b/vIsioueo/kick-assembler-vscode-ext)


# Quick Start using VICE for the C64

*For a more comprehensive setup, please consult the [guides](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/wikis/Guides.md) we have provided for each different system.*

- **Step 1.** Provide the Full Path and Filename to your [Kick Assembler Jar](http://www.theweb.dk/KickAssembler/Main.html#frontpage) file.

![](/images/new-setting-jar-border.png)

- **Step 2.** Provide the Full Path and Filename to your [Java](https://openjdk.java.net/install/) runtime.

![](/images/new-setting-java-border.png)

- **Step 3.** Provide the Full Path and Filename to your [VICE](https://sourceforge.net/projects/vice-emu/files/releases/) runtime.

![](/images/new-setting-vice-border.png)

- **Step 4.** Provide the Full Path and Filename to your [C64Debugger](https://sourceforge.net/projects/c64-debugger/files/) runtime.

![](/images/new-setting-debug-border.png)

- **Step 5.** Create a test source file.

![](/images/new-quick-add-file-border.gif)

- **Step 6.** Enter a simple program.

*Copy/Paste the program below or type it into your editor*

```
BasicUpstart(main)

* = $0810

main:

	inc $d021
	jmp main
```

![](/images/new-quick-code-border.gif)

- **Step 7.** Build and Run your program.

![](/images/new-quick-run-border.gif)

- **Step 8.** Build and Debug your program.

![](/images/new-debug-border.gif)

# Features

Here are some of the **features** that the extension provides.

- works with [many platforms](#platform-support) like Windows, MacOS and Linux
- support for [many different emulators](#emulator-support) like [VICE](http://vice-emu.sourceforge.net/), [Stella](https://stella-emu.github.io/), [MESEN](https://mesen.ca/) and more
- support for [debuggers] like [C64Debugger](https://sourceforge.net/projects/c64-debugger/)
- [syntax highlighting](#syntax-highlighting- 'Jump to Syntax Highlighting')
- [code completion](#code-completion- 'Jump to Code Completion') with intellisense
- [error checking](#error-checking- 'Jump to Error Checking')
- [hover support](#hover-support- 'Jump to Hover Support') including comments and values
- [code outlines](#code-outline- 'Jump to Code Outline')
- [code snippets](#code-snippets- 'Jump to Code Snippets')
- [code scoping](#code-scoping- 'Jump to Code Scoping')
- [directive support]() for imports
- Side Bar [memory view](#memory-view- 'Jump to Memory View')
- [breakpoints and log](#breakpoint- 'Jump to Breakpoints and Logs')
- and more:
	- illegal opcode support
	- binary output
	- plugins
	- VICE symbols
	- import library directories
	- code folding support
	- support for building cartridges

## Works On Multiple Platforms [#](#platform-support- 'Platform Support')
The extension works the same on any platform you choose.

![](/images/multi-platform.png)
## Support For Many Emulators [#](#emulator-support- 'Emulator Support')
You have complete control over the Emulator and the Options used when running your project. We have tested projects on many different Emulators with great success and we are testing more all the time.

![](/images/multi-emulator.png)
## Syntax Highlighting [#](#syntax-highlighting- 'Syntax Highlighting')
Make it easier to locate the different pieces of your code with built in syntax highlighting. Never again will you have to guess what you are looking at in your code.

![](/images/new-syntax-highlight-border.png)
## Code Completion [#](#code-completion- 'Code Completion')
The extension will show you every label, macro, function and opcode while you work. Works with currently open file as well as any files that you include in your code. Build your code libraries and be confident that the extension will help you find what you are looking for.

![](/images/new-auto-completion-border.gif)
## Error Checking [#](#error-checking 'Error Checking')
On-The-Fly error checking of your code that will inform you of incorrect opcodes, wrong labels, and other errors that will slow you down when coding. Be confident that when you build your program, that it is error free.

![](/images/new-syntax-errors-border.gif)

All errors are summarized in the Problems view for an added convenience.

![](/images/new-problems-view-border.png)
## Hover Support [#](#hover-support- 'Hover Support')
Hover support is available on almost every piece of code in your program. Add comments to your Macros and Functions, and have them show up when you hover over them elsewhere in your code. Numbers are automatically shown in many different forms like Hexadecimal and Binary for your convenience.

![](/images/new-hover-border.png)

Show the low and high byte values automatically.

![](/images/new-hover-values-border.gif)


## Code Outline [#](#code-outline- 'Code Outline')
See an outline of your program at a glance, and jump to any location just by clicking on it. No more searching through your code to figure out where that function or macro is located.

![](/images/new-outline-border.gif)

## Code Snippets [#](#code-snippets 'Code Snippets')
This is a powerful feature that will guide you when using Macros, Functions and other Built in Commands. See the parameters you need to enter as your type in your code.

![](/images/new-code-snippet-border.gif)

## Code Scoping [#](#code-scoping- 'Code Scoping')
Harness the power of Kick Assembler by utilizing Namespaces and Code Segments. The extensions helps you by keeping track of the scope of your code, and making sure you are not going outside your code boundaries.

![](/images/new-namespace-border.gif)

## Memory View [#](#memory-view- 'Memory View')
Visually see how your code is placed in memory, and optionally show how ROMS are overlayed with your code. Currently we have support for C64 with support for other platforms coming soon.

![](/images/new-memory-view-border.gif)

## Breakpoints & Logging
Use the built in capabilities of Visual Studio Code to help you add breakpoints to your code as well as logging. Export the breakpoints for use in VICE or C64Debugger.

![](/images/new-breakpoints-border.gif)

# Acknowledgements
Thanks to [SWOFFA](https://csdb.dk/scener/?id=984) for his work on the tmLanguage file for syntax highlighting from his [Sublime Package](https://github.com/Swoffa/SublimeKickAssemblerC64).

I would also like to recognize [Thomas Conté]() for his work on the original [vscode-kickassembler](https://github.com/tomconte/vscode-kickassembler) extension for VSCode that inspired me to start this project.

# Contributing
Bug reports, fixes, and other changes are welcomed. The [repository](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext) is on [GitLab](https://gitlab.com), and issues and pull requests are accepted. Check the [contribute](CONTRIBUTE.md) file on information of what the project needs, and how to run the extension locally for development and testing.

# Heroes
Thanks to the following for their contributions to this extension. Apologies if I missed someone, please let me know and I will add you here.

- [**@lubber**](https://csdb.dk/scener/?id=124) for his enourmous support and help with things like hover support, breakpoint support, code completion support, really the list is endless. Without his help this extension would not be what it is today.
- [**@4ch1m**](https://gitlab.com/4ch1m) for his help in having us use the statusbar instead of spamming everyone with popups.
- [**@ki-bo**](https://gitlab.com/ki-bo) for help with adding .break support.
- [**@NervousEnergy**](https://gitlab.com/NervousEnergy) for help with cartridge build support.
- [**@uharries**](https://gitlab.com/uharries) for tab after instruction support.
